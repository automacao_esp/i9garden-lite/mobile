import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';

import { AgendaComponent } from './agenda.component';

@NgModule({
  
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
  ],
  declarations: [AgendaComponent]
})
export class HomePageModule {}