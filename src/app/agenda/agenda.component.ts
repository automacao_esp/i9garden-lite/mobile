import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-agenda',
  templateUrl: './agenda.component.html',
  styleUrls: ['./agenda.component.scss'],
})
export class AgendaComponent implements OnInit {

  public data : string;
  public tempo : number;

  constructor(private modalCtrl : ModalController) { 
    this.data = new Date().toISOString();
    this.tempo = 10; //tempo padrão para irrigar
  }

  ngOnInit() {}

  public fechar() : void {
    this.modalCtrl.dismiss({
      close : true
    });
  }

  public salvar() : void{
    let time = new Date(this.data);    
    this.modalCtrl.dismiss({
      close : false,
      hora : `${("0" + time.getHours()).slice(-2)}:${("0" + time.getMinutes()).slice(-2)}:${("0" + time.getSeconds()).slice(-2)}`,
      tempo: this.tempo
    });
  }

  public dateChanged(e : any):void{
    this.data = e.detail.value
  }

  public timeChanged(e : any): void{
    if(e.detail.value) this.tempo = e.detail.value;
  }
}
