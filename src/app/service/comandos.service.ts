import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ComandosService {

  private api: string = "http://192.168.4.1";
  constructor(private http: HttpClient) {

  }

  public start(): Observable<any> {
    return this.http.post(`${this.api}/start`, []);
  }

  public stop(): Observable<any> {
    return this.http.post(`${this.api}/stop`, []);
  }

  public getHora(): Observable<any> {
    return this.http.get(`${this.api}/get/clock`, {});
  }

  public sincronizarRelogio(): Observable<any> {
    //  let httpParams = new HttpParams()
    //    .set('hora', hora.toString())
    //    .set('minuto', minuto.toString())
    //    .set('segundo', segundo.toString());

    const data = new Date();
    let hora = data.getHours();
    let minuto = data.getMinutes();
    let segundo = data.getSeconds();
    return this.http.post(`${this.api}/set/clock?hora=${hora}&minuto=${minuto}&segundo=${segundo}`, { });
  }

  public getAgenda(): Observable<any> {
    return this.http.get(`${this.api}/get/agenda`);
  }



  public setAgenda(agenda : any[]): Observable<any>{
    return this.http.post(`${this.api}/set/agenda?agenda=${JSON.stringify(agenda)}`, { });
  }
}
