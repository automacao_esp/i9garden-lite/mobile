import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { ToastController, ModalController, LoadingController } from '@ionic/angular';

import { ComandosService } from '../service/comandos.service';
import { AgendaComponent } from '../agenda/agenda.component';
import { isNumber } from 'util';

interface Tarefa {
  hora: string,
  tempo: number
}

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss']
})

export class HomePage {

  public irrigar: boolean = false;
  public hora: Date;
  public agenda: Tarefa[] = [];
  public conectado: boolean = false;
  public esperandoResposta: boolean = false;
  public exibirErroConexao: boolean = false;

  constructor(
    public esp8266: ComandosService,
    public toastController: ToastController,
    public modalController: ModalController,
    public loadingController: LoadingController) {

  }

  ngOnInit() {
    this.conectar();
  }


  public async conectar() {
    this.exibirErroConexao = false;
    const loading = await this.loadingController.create({
      message: 'Conectando ...'
    });
    await loading.present();

    this.esp8266.sincronizarRelogio().subscribe({
      next: () => {
        this.conectado = true;

        this.esp8266.getAgenda().subscribe({
          next: (agenda: Tarefa[]) => {
            if (agenda) {
              agenda.sort((a, b) => { return a.hora.localeCompare(b.hora) });
              this.agenda = agenda;
            }
            loading.dismiss();
          },
          error: (_erro: any) => {
            loading.dismiss();
            this.toast(false, "Falha ao carregar a agenda")
          }
        })
      },

      error: (_erro: any) => {
        this.toast(false, "Falha ao conectar no dispositivo");
        this.exibirErroConexao = true;
        loading.dismiss();
      }
    });

  }

  private async toast(sucesso: boolean, message: string) {
    const toast = await this.toastController.create({
      color: (sucesso) ? "success" : "danger",
      message: message,
      duration: 2000
    });
    await toast.present();
  }


  public irrigarManualmente(event: any): void {
    let observer: Observable<any>;
    observer = (this.irrigar) ? this.esp8266.start() : this.esp8266.stop();

    this.esperandoResposta = true;
    observer.subscribe({
      next: (res: any) => {
        this.toast(true, res.msg);
        this.esperandoResposta = false;
      },
      error: () => {
        this.toast(false, "Falha ao enviar o comando...");
        this.esperandoResposta = false;
      },
    })
  }


  private salvarAgenda() {
    this.esperandoResposta = true;
    this.esp8266.setAgenda(this.agenda).subscribe({
      next: () => {
        this.toast(true, "Agenda de irrigação salva....");
        this.esperandoResposta = false;
      },
      error: () => {
        this.toast(true, "Falha ao gravar horários de irrigação....");
        this.esperandoResposta = false;
      }
    });
  }

  public async adicionarHorario(event: any) {
    const modal = await this.modalController.create({
      component: AgendaComponent
    });
    await modal.present();

    const { data } = await modal.onWillDismiss();
    if (!data.close && data.hora && data.tempo) {
      this.agenda.push({ hora: data.hora, tempo: data.tempo })
      this.agenda.sort((a, b) => { return a.hora.localeCompare(b.hora) });
      this.salvarAgenda();
    }
  }


  public remover(index: number): void {
    if (isNumber(index)) {
      this.agenda.splice(index, 1);
      this.agenda.sort((a, b) => { return a.hora.localeCompare(b.hora) });
      this.salvarAgenda();
    }
  }

}
